import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolRoutingModule } from './school-routing.module';
import {SchoolComponent} from '../../components/school/school.component';
import {MaterialModule} from '../../shared/material.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { SchoolHeaderComponent } from '../../components/school/components/navigation/school-header/school-header.component';
// import {SchoolHeaderComponent} from '../../components/school/navigation/school-header/school-header.component';


@NgModule({
  declarations: [
    SchoolComponent,
    SchoolHeaderComponent,
    // SchoolHeaderComponent,
  ],
  imports: [
    CommonModule,
    SchoolRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SchoolModule { }
