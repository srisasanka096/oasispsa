import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SchoolComponent} from '../../components/school/school.component';


const routes: Routes = [
  {
    path: '',
    component: SchoolComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'administration'
      }, {
        path: 'administration',
        loadChildren: () => import('../admin/admin.module').then(m => m.AdminModule),
      }, {
        path: 'academics',
        loadChildren: () => import('../academics/academics.module').then(m => m.AcademicsModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolRoutingModule { }
