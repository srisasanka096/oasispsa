import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { SchoolDetailsComponent } from '../../components/school/components/admin/school-details/school-details.component';
import { AdminComponent } from '../../components/school/components/admin/admin.component';
import {MaterialModule} from '../../shared/material.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [SchoolDetailsComponent, AdminComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AdminModule { }
