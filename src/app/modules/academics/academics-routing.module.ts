import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AcademicsComponent} from '../../components/school/components/academics/academics.component';
import {ClassDetailsComponent} from '../../components/school/components/academics/class-details/class-details.component';


const routes: Routes = [
  {
    path: '',
    component: AcademicsComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'class-details'
      }, {
        path: 'class-details',
        component: ClassDetailsComponent,
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicsRoutingModule { }
