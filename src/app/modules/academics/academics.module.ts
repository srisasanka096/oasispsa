import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcademicsRoutingModule } from './academics-routing.module';
import { AcademicsComponent } from '../../components/school/components/academics/academics.component';
import { ClassDetailsComponent } from '../../components/school/components/academics/class-details/class-details.component';
import {MaterialModule} from '../../shared/material.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AcademicsComponent, ClassDetailsComponent],
  imports: [
    CommonModule,
    AcademicsRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AcademicsModule { }
