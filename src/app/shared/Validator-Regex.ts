export class ValidatorRegex {
  AlphaPattern = '[a-zA-Z]*';
  NumericPattern = '[0-9]*';
  AlphaNumericPattern = '[a-zA-Z0-9]*';
  DecimalCommaPattern = '[0-9.,]*';
  DecimalPattern = '[0-9.]*';
  MobileNumber = '[+0-9]*';
  TelephoneNumber = '[+0-9]*';
  AlphaNumericWithWhiteSpace = '[a-zA-Z0-9 ]*';
  noOnlyWhiteSpaces = '.*[^ ].*';
  AlphaWithWhiteSpace = '[a-zA-Z ]*';
  AlphaWithWhiteSpaceAmp = '[a-zA-Z &.]*';
  AlphaDotWithWhiteSpace = '[a-zA-Z .]*';
  AlphaDot = '[a-zA-Z.]*';
  NumericCommaPattern = '[0-9,]*';
  CustomEmail = '^[a-z0-9A-Z.]+@[a-z0-9A-Z._]+[.]+[a-zA-Z]{2,}$';
}
