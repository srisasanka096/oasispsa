import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-school-header',
  templateUrl: './school-header.component.html',
  styleUrls: ['./school-header.component.scss']
})
export class SchoolHeaderComponent implements OnInit {

  constructor() { }

  administrationMenuList: Array<any> = [];
  academicsMenuList: Array<any> = [];

  ngOnInit(): void {
    this.schoolHeaderDefaultDetails();
  }

  schoolHeaderDefaultDetails() {
    this.administrationMenuList = [
      {
        path: '/school/administration/school-details',
        name: 'School Details',
        icon: 'person'
      },
    ];
    this.academicsMenuList = [
      {
        path: '/school/academics/class-details',
        name: 'Class Details',
        icon: 'person'
      },
    ];
  }
}
