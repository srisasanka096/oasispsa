import {Component, OnInit} from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-under-construction',
  templateUrl: './under-construction.component.html',
  styleUrls: ['./under-construction.component.scss']
})
export class UnderConstructionComponent implements OnInit {
  title = 'SchoolManagementSystem';

  ngOnInit() {
    function makeTimer() {
         //  Define end Time When you want to constuction to be Done.
          let endTime: any = new Date('01 july 2020 9:56:00 GMT+01:00');
          endTime = (Date.parse(endTime) / 1000);

          let now: any = new Date();
          now = (Date.parse(now) / 1000);


          const timeLeft = endTime - now;

          const days = Math.floor(timeLeft / 86400);
          let hours = Math.floor((timeLeft - (days * 86400)) / 3600);
          let minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
          let seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

          if (hours < 10) { hours = 0 + hours; }
          if (minutes < 10) { minutes = 0 + minutes; }
          if (seconds < 10) { seconds = 0 + seconds; }

          $('#days').html(days);
          $('#hours').html(hours);
          $('#minutes').html(minutes);
          $('#seconds').html(seconds);
      }
    setInterval(() => {
      makeTimer();
    }, 1000);

    $('.simpleslide100').each(function() {
      const delay = 5000;
      const speed = 1000;
      const itemSlide = $(this).find('.simpleslide100-item');
      let nowSlide = 0;

      $(itemSlide).hide();
      $(itemSlide[nowSlide]).show();
      nowSlide++;
      if (nowSlide >= itemSlide.length) {nowSlide = 0; }

      setInterval(() => {
          $(itemSlide).fadeOut(speed);
          $(itemSlide[nowSlide]).fadeIn(speed);
          nowSlide++;
          if (nowSlide >= itemSlide.length) {nowSlide = 0; }
      }, delay);
  });

  }

}
