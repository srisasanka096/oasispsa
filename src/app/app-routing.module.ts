import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {UnderConstructionComponent} from './components/under-construction/under-construction.component';
import {AuthGuard} from './guards/auth/auth.guard';


const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  }, {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
  }, {
    path: 'school',
    loadChildren: () => import('./modules/school/school.module').then(m => m.SchoolModule),
    canActivate: [AuthGuard],
  }, {
    path: '',
    pathMatch: 'full',
    redirectTo: 'under-construction'
  }, {
    path: 'under-construction',
    component: UnderConstructionComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
