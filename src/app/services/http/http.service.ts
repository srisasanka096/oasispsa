import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  getData(url: string) {
    return this.httpClient.get(this.prepareUrl(url));
  }

  postData(url: string, body: object) {
    return this.httpClient.post(this.prepareUrl(url), body);
  }

  prepareUrl(url: string) {
    return environment.serverUrl + url;
  }
}
