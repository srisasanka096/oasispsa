# OasisPsa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Steps to follow:
1. Run `npm install` to install the dependencies after cloning the project from version control.

2. Run `npm start` to start the server.

3. Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build. Use `--aot` flag for adding ahead of time compilation to build.

4. un `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

5.  Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

6.  Dependency Documents, css, js, images, logo, icons are placed in assets.

7. In app, 
    1. components are created in components folder.
    
    2. modules are created in modules folder. modules also contains routing modules. In module file, components are declared, other modules can be imported or exported. To use as modals, components are declared in entry components of the module file.
    
    3. routes are declared in the routing module file (Example: app-routing.module.ts).
    
    4. auth guards and role guards are declared in guards directory.
    
    5. In Helpers directory, interceptors like http-request, http-error, token interceptors are utilized.
    
    6. In Services Directory, services are declared. http services contains http grt and post methods. Http Calls are to be used in components and other services by utilizing http service.
    
    7. In Shared Directory, shared/common files for the angular application are declared. Example: Validator Regex contains pattern regular expressions; Date Adaptor contains date format. and material module contains Angular material Module imports and exports which are imported by other modules.

8. To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
